/*jshint esversion: 6 */
//This is the entry point JavaScript file.
//Webpack will look at this file first, and then check
//what files are linked to it.
console.log("Hello world");

function question2( minN, maxN, num){
  if (minN < num && num < maxN)
  console.log("part 3");
}

//global variables 
let count = 0;

//I should check all 5 questions 
//each circle will change color depending on the answers
//blue means wrong 
//green means correct 
//ouput a score of # of correct answers to as a popup 
function checkAllQuiz() {
  count = 0;
  checkOneQuiz("#q1", 2, 0);
  checkOneQuiz("#q2", 70, 1);
  checkOneQuiz("#q3" , 9 , 2 );
  checkOneQuiz("#q4" , 27 , 3);
  checkOneQuiz("#q5" , 10 , 4);
  // alert("# of corret ans " + count ); 
  document.querySelector("#ans").innerHTML = "# of correct answers " + count; 
}


function checkOneQuiz(id, ans, question){
  let userAnswer = Number(document.querySelector(id).value);
  let circles = document.querySelectorAll(".circle");
  console.log(circles);
  console.log(userAnswer);
  if (userAnswer == ans ) {
    //change color to green
    circles[question].setAttribute("fill", "green");
    count = count + 1;
  }
  else {
    //change color to blue 
    circles[question].setAttribute("fill", "blue"); 
  }
}