let baseString = "TestPage"
function populatePage() {
    let containers = document.querySelectorAll(".container"); //get all the containers

    //Part 1: Rewrite the following code using a for/of loop. 
    //Trivial example: This code capitalizes what is in the HTML file
    //and counts the number of charcters
    //reference for/of loops: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/for...of
    //reference for loops: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/for
    let count = 0;
    count = count + containers[0].innerHTML.length;
    containers[0].innerHTML = containers[0].innerHTML.toUpperCase();
    count = count + containers[1].innerHTML.length;
    containers[1].innerHTML = containers[1].innerHTML.toUpperCase();
    count = count + containers[2].innerHTML.length;
    containers[2].innerHTML = containers[2].innerHTML.toUpperCase();
    count = count + containers[3].innerHTML.length;
    containers[3].innerHTML = containers[3].innerHTML.toUpperCase();
    count = count + containers[4].innerHTML.length;
    containers[4].innerHTML = containers[4].innerHTML.toUpperCase();
    count = count + containers[5].innerHTML.length;
    containers[5].innerHTML = containers[5].innerHTML.toUpperCase();
    count = count + containers[6].innerHTML.length;
    containers[6].innerHTML = containers[6].innerHTML.toUpperCase();
    count = count + containers[7].innerHTML.length;
    containers[7].innerHTML = containers[7].innerHTML.toUpperCase();

    //Output Part 1: You do not need to change this.
    let output = document.querySelector("#output");
    output.innerHTML = "Number of characters:" + count;

    //Part 2: Rewrite the following using a loop and a function
    let heights = [4, 5, 3, 7, 6, 10,-2];
    let pointString = "";//start with an empy string
    let x = 0;

    //Replace the following code with a for/of loop.
    //In this loop, call a function that you will write to 
    //help create the string of points

    x = x + 10;
    y = 200 - heights[0] * 20;
    //Take the next line of code and make it a function
    //You function will take in, x, y, and the existing string ( 3 parameters into total)
    //and return the new string with the ponts added.
    //Reference: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Functions
    pointString = pointString + x + "," + y + " ";

    x = x + 10;
    y = 200 - heights[1] * 20;
    pointString = pointString + x + "," + y + " ";

    x = x + 10;
    y = 200 - heights[2] * 20;
    pointString = pointString + x + "," + y + " ";

    x = x + 10;
    y = 200 - heights[3] * 20;
    pointString = pointString + x + "," + y + " ";

    x = x + 10;
    y = 200 - heights[4] * 20;
    pointString = pointString + x + "," + y + " ";

    x = x + 10;
    y = 200 - heights[5] * 20;
    pointString = pointString + x + "," + y + " ";

    x = x + 10;
    y = 200 - heights[6] * 20;
    pointString = pointString + x + "," + y + " ";
    let output2 = document.querySelector("#output2");
    output2.setAttribute("points", pointString);


    let newThings = []
    //Part 3:  Creating things with loop
    // on the html page with loop.
    //mananually, there are other ways of course.
    let data = [30, 70, 110, 150];
    let svgString = '<svg width="500" height="500">';
    x= 0;

    x = data[0];
    circleString = '<circle cx="'+x+'"'+' cy="'+250+'" r="'+20+'" fill="black" />';
    svgString += circleString;

    x = data[1];
    circleString = '<circle cx="'+x+'"'+' cy="'+250+'" r="'+20+'" fill="black" />';
    svgString += circleString;

    x = data[2];
    circleString = '<circle cx="'+x+'"'+' cy="'+250+'" r="'+20+'" fill="black" />';
    svgString += circleString;

    x = data[3];
    circleString = '<circle cx="'+x+'"'+' cy="'+250+'" r="'+20+'" fill="black" />';
    svgString += circleString;

    svgString += "</svg>"
    let output3 = document.querySelector("#output3")
    output3.innerHTML += svgString;

}


function growBoxes() {
    //get all 3 inputs 
    // boxes based on the inputs 
    
    //eg 3 boxes 
    //gap = 50 
    //initial size = 50 
    //growth: 10 
    //1st box:  <rect width="50" height="50" x="0" y ="100" fill="black">
    //2nd box:  <rect width="60" height="60" x="100" y ="100" fill="black">
    //3rd box:  <rect width="70" height="70" x="210" y ="100" fill="black">
    //making a string out of all the boxes 
    //str = "<rect width="50" height="50" x="0" y ="100" fill="black"><rect width="60" height="60" x="100" y ="100" fill="black"><rect width="70" height="70" x="210" y ="100" fill="black">"
    //put the string inside your svg 
    //somehow get the svg 
    //canvas.innerHTML = new STring
    let size = Number(document.querySelector("#size").value);
    let boxes = Number(document.querySelector("#boxes").value);
    let growth = Number(document.querySelector("#growth").value);
    let canvas = document.querySelector("#canvas");

    let boxString = "";
    let boxSize = size ;
    let boxGap = 0; 
    let gap = 50; 
    for (let box = 0; box < boxes; box = box + 1) {
        boxString = boxString + makeBox(boxSize, boxGap);
        boxGap = boxGap + boxSize + gap ; 
        boxSize = boxSize + growth;
    }
    canvas.innerHTML = boxString;
    console.log(boxString);
}

//this function creates a string like <rect width="50" height="50" x="0" y ="100" fill="black">
//but with proper values 
//boxSize: size of width and height 
//boxGap: x value 
//return a string like <rect width="50" height="50" x="0" y ="100" fill="black">
function makeBox(boxSize, boxGap) {
    let str = ""+ "<rect width=\"" +boxSize+ "\" height=\""+boxSize+"\" x=\""+boxGap+"\" y=\"100\" fill=\"black\"/>"  ;
    return str; 
}



